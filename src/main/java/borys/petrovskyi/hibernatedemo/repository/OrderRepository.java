package borys.petrovskyi.hibernatedemo.repository;

import borys.petrovskyi.hibernatedemo.entity.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Kurator on 07.06.2017.
 */
@Repository
public interface OrderRepository extends JpaRepository<Order, Long> {
    @Query("select o from Order o where o.userId = :userid")
    List<Order> findOrdersByUserId(@Param("userid") Integer userid);
}
