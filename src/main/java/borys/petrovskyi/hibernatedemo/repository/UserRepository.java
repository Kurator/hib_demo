package borys.petrovskyi.hibernatedemo.repository;

import borys.petrovskyi.hibernatedemo.dinterface.ICloseSession;
import borys.petrovskyi.hibernatedemo.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Created by Kurator on 07.06.2017.
 */
@Repository
public interface UserRepository extends JpaRepository<User, Long>, ICloseSession {
    @Query("select u from User u where u.userName = :name")
    User findByName(@Param("name") String name);
}
