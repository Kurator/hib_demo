package borys.petrovskyi.hibernatedemo.controller;

import borys.petrovskyi.hibernatedemo.dao.UserDAO;
import borys.petrovskyi.hibernatedemo.entity.Order;
import borys.petrovskyi.hibernatedemo.entity.OrderIdentifier;
import borys.petrovskyi.hibernatedemo.entity.User;
import borys.petrovskyi.hibernatedemo.service.OrderService;
import borys.petrovskyi.hibernatedemo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import java.util.List;
import java.util.Set;

/**
 * Created by Kurator on 07.06.2017.
 */
@Controller
public class TestController {
    @Autowired
    private UserService service;
    @Autowired
    private OrderService oService;

    public void startTest() {
        test1();
        test2();
        testLambda();
        testLambdaAndStream();
        testLambdaAndStream2();
        service.closeSession();
    }

    private void test1() {
        System.out.println("TEST 1 HAS BEEN STARTED");
        System.out.println("======================================");
        System.out.println("Step 1");
        User user = service.getByName("Kuret");
        System.out.println("Step 2");
        Set<Order> orderSet = user.getUserOrders();
        System.out.println("Step 3");
        for (Order order: orderSet) {
            System.out.println("Step 4");
            System.out.println(user);
            System.out.println(order);
        }
        System.out.println("TEST 1 HAS BEEN ENDED");
    }

    private void test2() {
        System.out.println("TEST 2 HAS BEEN STARTED");
        System.out.println("======================================");
        System.out.println("Step 1");
        List<User> userList = service.getAllUsers();
        System.out.println("Step 2");
        for (User userFromList: userList) {
            System.out.println("Step 3");
            for (Order order : userFromList.getUserOrders()) {
                System.out.println("Step 4");
                System.out.println(userFromList.getUserName());
                System.out.println(order);
            }
        }
        System.out.println("TEST 2 HAS BEEN ENDED");
    }

    private void testLambda() {
        System.out.println("TEST 3 HAS BEEN STARTED");
        System.out.println("======================================");
        // Potential NPE when firstName == null
        TestFuncInterface testFuncInterface = (firstName, secondName) -> firstName.equals(secondName);
        User user = service.getByName("Kuret");
        String userName = user.getUserName();
        System.out.println(" " + testFuncInterface.comp(userName, "Kuret"));
        System.out.println(" " + testFuncInterface.comp(userName, "Borys"));
        System.out.println("TEST 3 HAS BEEN ENDED");
    }

    private void testLambdaAndStream() {
        System.out.println("TEST 4 HAS BEEN STARTED");
        System.out.println("======================================");
        User user = service.getByName("Borys");
        Set<Order> orders = user.getUserOrders();
        System.out.println("Show stream content:");
        orders.stream().forEach(System.out::println);
        System.out.println("Here is some stream intermediate operations:");
        System.out.println("Filtered by OrderID: ");
        orders.stream().filter(order -> 31 == order.getOrderId()).forEach(System.out::println);
        System.out.println("Skip 2 elements: ");
        orders.stream().skip(2).forEach(System.out::println);
        System.out.println("Distinct stream (all elements are unique, so nothing to remove): ");
        orders.stream().distinct().forEach(System.out::println);
        // Will change base collection, all the results below are corrected with new values
        System.out.println("Peek (decrease order ID) stream: ");
        orders.stream().peek(order -> order.setOrderId(order.getOrderId()-10)).forEach(System.out::println);
        System.out.println("Limit stream for 2 elements: ");
        orders.stream().limit(2).forEach(System.out::println);
        System.out.println("Sort stream with comparator: ");
        orders.stream().sorted((order1, order2) -> order1.getId().compareTo(order2.getId())).forEach(System.out::println);
        System.out.println("Here is some stream terminal operations:");
        System.out.println("Max value: " + orders.stream().max((o1, o2) -> o1.getOrderId().compareTo(o2.getOrderId())));
        System.out.println("Mix value: " + orders.stream().min((o1, o2) -> o1.getOrderId().compareTo(o2.getOrderId())));
        System.out.println("FindAny value: " + orders.stream().findAny());
        System.out.println("FindFirst value: " + orders.stream().findFirst());
        System.out.println("Count orders: " + orders.stream().count());
        System.out.println("Any match, true: " + orders.stream().anyMatch(order -> order.getId().equals(new OrderIdentifier(6, "inv_006"))));
        System.out.println("Any match, false: " + orders.stream().anyMatch(order -> order.getId().equals(new OrderIdentifier(0, "_"))));
        System.out.println("All match, true: " + orders.stream().allMatch(order -> order.getUserId()==2));
        System.out.println("All match, false: " + orders.stream().allMatch(order -> order.getUserId()==0));
        System.out.println("TEST 4 HAS BEEN ENDED");
    }

    private void testLambdaAndStream2() {
        System.out.println("TEST 5 HAS BEEN STARTED");
        System.out.println("======================================");
        List<Order> ordersList = oService.getOrdersByUserId(2);
        System.out.println("Show stream content:");
        ordersList.stream().forEach(System.out::println);
        System.out.println("Distinct stream (updated test to check distinct comparison): ");
        ordersList.stream().distinct().forEach(System.out::println);
        System.out.println("TEST 5 HAS BEEN ENDED");
    }

    public interface TestFuncInterface {
        boolean comp(String firstName, String secondName);
    }
}
