package borys.petrovskyi.hibernatedemo.controller;

import borys.petrovskyi.hibernatedemo.entity.Address;
import borys.petrovskyi.hibernatedemo.entity.Order;
import borys.petrovskyi.hibernatedemo.entity.OrderIdentifier;
import borys.petrovskyi.hibernatedemo.entity.User;
import borys.petrovskyi.hibernatedemo.service.OrderService;
import borys.petrovskyi.hibernatedemo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.*;

/**
 * Created by Kurator on 09.06.2017.
 */
@RestController
public class TestRestController {
    private static final String APP_APPENDER = "hib_demo";
    private static final String APP_APPENDER_WITH_SEPARATOR = APP_APPENDER + "/";
    @Autowired
    private UserService userService;
    @Autowired
    private OrderService orderService;

    @RequestMapping(APP_APPENDER)
    public String showAppInfoWOSep(HttpServletRequest request) {
        return showAppInfo(request);
    }

    @RequestMapping(APP_APPENDER_WITH_SEPARATOR)
    public String showAppInfo(HttpServletRequest request) {
        String baseLink = request.getRequestURL().toString();
        if (!baseLink.endsWith("/")) {
            baseLink += "/";
        }
        StringBuilder result = new StringBuilder();
        result.append("Instruction:");
        result.append("<p>");
        result.append(baseLink + "getUser/{userId} - where userId is Integer index of requested user. Show user info");
        result.append("<p>");
        result.append(baseLink + "getOrder/{orderId} - where orderId is Integer index of requested order. Show order info");
        result.append("<p>");
        result.append(baseLink + "getAllUsers - Show info about all the users from the users table");
        result.append("<p>");
        result.append(baseLink + "getAllOrders - Show info about all the orders from the orders table");
        result.append("<p>");
        result.append(baseLink + "test1 - run test #1. Hibernate database test (additional info is available in logs)");
        result.append("<p>");
        result.append(baseLink + "test2 - run test #2. Hibernate n+1 problem (additional info is available in logs)");
        result.append("<p>");
        result.append(baseLink + "test3 - run test #3. Lambda test, functional interface test");
        result.append("<p>");
        result.append(baseLink + "test4 - run test #4. Couple of tests with StreamAPI");
        result.append("<p>");
        result.append(baseLink + "test5 - run test #5. Additional test with StreamAPI (distinct method)");
        result.append("<p>");
        result.append(baseLink + "test6 - analog of test #2. Eager fetch type used");
        result.append("<p>");
        result.append(baseLink + "test7 - Multithreading test with ExecutorService");
        result.append("<p>");
        result.append(baseLink + "testAll - run all 5 tests in a row");
        return result.toString();
    }

    @RequestMapping(APP_APPENDER_WITH_SEPARATOR + "getUser/{userId}")
    public String getUserInfoById(@PathVariable Long userId) {
        return userService.getUserById(userId).toString();
    }

    @RequestMapping(APP_APPENDER_WITH_SEPARATOR + "getOrder/{orderId}")
    public String getOrderInfoById(@PathVariable Long orderId) {
        return orderService.getOrderById(orderId).toString();
    }

    @RequestMapping(APP_APPENDER_WITH_SEPARATOR + "getAllUsers")
    public String getAllUsers() {
        List<User> usersList = userService.getAllUsers();
        StringBuilder result = new StringBuilder();
        for (User u: usersList) {
            result.append(u.toString());
        }
        if (result.length() > 0) {
            return result.toString();
        } else {
            return "No users to show";
        }
    }

    @RequestMapping(APP_APPENDER_WITH_SEPARATOR + "getAllOrders")
    public String getAllOrders() {
        List<Order> ordersList = orderService.getAll();
        StringBuilder result = new StringBuilder();
        for (Order o: ordersList) {
            result.append(o.toString());
        }
        if (result.length() > 0) {
            return result.toString();
        } else {
            return "No orders to show";
        }
    }

    @RequestMapping(APP_APPENDER_WITH_SEPARATOR + "test1")
    public String test1() {
        StringBuilder result = new StringBuilder();
        result.append("TEST 1 HAS BEEN STARTED");
        result.append("<p>");
        result.append("Please notice in logs how many requests Hibernate sent to the DB");
        result.append("<p>");
        User user = userService.getByName("Kuret");
        Set<Order> orderSet = user.getUserOrders();
        for (Order order: orderSet) {
            result.append(user);
            result.append("<p>");
            result.append(order);
            result.append("<p>");
        }
        result.append(("TEST 1 HAS BEEN ENDED"));
        return result.toString();
    }

    @RequestMapping(APP_APPENDER_WITH_SEPARATOR + "test2")
    public String test2() {
        StringBuilder result = new StringBuilder();
        result.append("TEST 2 HAS BEEN STARTED");
        result.append("<p>");
        result.append("Please notice in logs how many requests Hibernate sent to the DB");
        result.append("<p>");
        List<User> userList = userService.getAllUsers();
        for (User userFromList: userList) {
            for (Order order : userFromList.getUserOrders()) {
                result.append(userFromList.getUserName());
                result.append("<p>");
                result.append(order);
                result.append("<p>");
            }
        }
        result.append("TEST 2 HAS BEEN ENDED");
        return result.toString();
    }

    @RequestMapping(APP_APPENDER_WITH_SEPARATOR + "test3")
    public String test3() {
        StringBuilder result = new StringBuilder();
        result.append("TEST 3 HAS BEEN STARTED");
        result.append("<p>");
        result.append("We requested a single record by username 'Kuret' and compare this username with the same one and with the different one.");
        result.append("<p>");
        // Potential NPE when firstName == null
        TestController.TestFuncInterface testFuncInterface = (firstName, secondName) -> firstName.equals(secondName);
        User user = userService.getByName("Kuret");
        String userName = user.getUserName();
        result.append("Comparison with 'Kuret': " + testFuncInterface.comp(userName, "Kuret"));
        result.append("<p>");
        result.append("Comparison with 'Borys' " + testFuncInterface.comp(userName, "Borys"));
        result.append("<p>");
        result.append("TEST 3 HAS BEEN ENDED");
        return result.toString();
    }

    @RequestMapping(APP_APPENDER_WITH_SEPARATOR + "test4")
    public String test4() {
        StringBuilder result = new StringBuilder();
        result.append("TEST 4 HAS BEEN STARTED");
        result.append("<p>");
        User user = userService.getByName("Borys");
        Set<Order> orders = user.getUserOrders();
        result.append("Show stream content:");
        result.append("<p>");
        orders.stream().forEach(result::append);
        result.append("Here is some stream intermediate operations:");
        result.append("<p>");
        result.append("Filtered by OrderID==31: ");
        result.append("<p>");
        orders.stream().filter(order -> 31 == order.getOrderId()).forEach(result::append);
        result.append("<p>");
        result.append("Skip 2 elements: ");
        result.append("<p>");
        orders.stream().skip(2).forEach(result::append);
        result.append("<p>");
        result.append("Distinct stream (all elements are unique, so nothing to remove): ");
        result.append("<p>");
        orders.stream().distinct().forEach(result::append);
        result.append("<p>");
        // Will change base collection, all the results below are corrected with new values
        result.append("Peek (decrease order ID) stream: ");
        result.append("<p>");
        orders.stream().peek(order -> order.setOrderId(order.getOrderId()-10)).forEach(result::append);
        result.append("<p>");
        result.append("Limit stream for 2 elements: ");
        result.append("<p>");
        orders.stream().limit(2).forEach(result::append);
        result.append("<p>");
        result.append("Sort stream with comparator: ");
        result.append("<p>");
        orders.stream().sorted((order1, order2) -> order1.getId().compareTo(order2.getId())).forEach(result::append);
        result.append("<p>");
        result.append("Here is some stream terminal operations:");
        result.append("<p>");
        result.append("Max value: " + orders.stream().max((o1, o2) -> o1.getOrderId().compareTo(o2.getOrderId())));
        result.append("<p>");
        result.append("Mix value: " + orders.stream().min((o1, o2) -> o1.getOrderId().compareTo(o2.getOrderId())));
        result.append("<p>");
        result.append("FindAny value: " + orders.stream().findAny());
        result.append("<p>");
        result.append("FindFirst value: " + orders.stream().findFirst());
        result.append("<p>");
        result.append("Count orders: " + orders.stream().count());
        result.append("<p>");
        result.append("Any match (when correct result presents in stream): " + orders.stream().anyMatch(order -> order.getId().equals(new OrderIdentifier(6, "inv_006"))));
        result.append("<p>");
        result.append("Any match (when correct result us not presents in stream): " + orders.stream().anyMatch(order -> order.getId().equals(new OrderIdentifier(0, "_"))));
        result.append("<p>");
        result.append("All match (when all the results match): " + orders.stream().allMatch(order -> order.getUserId()==2));
        result.append("<p>");
        result.append("All match (when one or all results are mismatched): " + orders.stream().allMatch(order -> order.getUserId()==0));
        result.append("<p>");
        result.append("TEST 4 HAS BEEN ENDED");
        return result.toString();
    }

    @RequestMapping(APP_APPENDER_WITH_SEPARATOR + "test5")
    public String test5() {
        StringBuilder result = new StringBuilder();
        result.append("TEST 5 HAS BEEN STARTED");
        result.append("<p>");
        List<Order> ordersList = orderService.getOrdersByUserId(2);
        result.append("Show stream content:");
        result.append("<p>");
        ordersList.stream().forEach(result::append);
        result.append("<p>");
        result.append("Distinct stream (updated test to check distinct comparison): ");
        result.append("<p>");
        ordersList.stream().distinct().forEach(result::append);
        result.append("<p>");
        result.append("TEST 5 HAS BEEN ENDED");
        return result.toString();
    }

    @RequestMapping(APP_APPENDER_WITH_SEPARATOR + "test6")
    public String test6() {
        StringBuilder result = new StringBuilder();
        result.append("TEST 6 HAS BEEN STARTED");
        result.append("<p>");
        result.append("Please notice in logs how many requests Hibernate sent to the DB");
        result.append("<p>");
        List<User> userList = userService.getAllUsers();
        for (User userFromList: userList) {
            for (Address address : userFromList.getUserAddresses()) {
                result.append(userFromList.getUserName());
                result.append("<p>");
                result.append(address);
                result.append("<p>");
            }
        }
        result.append("TEST 6 HAS BEEN ENDED");
        return result.toString();
    }

    @RequestMapping(APP_APPENDER_WITH_SEPARATOR + "test7")
    public String testMultithreading() {
        StringBuilder result = new StringBuilder();
        ExecutorService service = Executors.newCachedThreadPool();
        int threadsPool = 10;
        List<Future> futures = new ArrayList<Future>(10);
        for (int i = 0; i < threadsPool; i++) {
            final int finalI = i;
            futures.add(service.submit(new Callable<String>() {
                @Override
                public String call() throws Exception {
                    return "Callable#" + finalI + ": " +userService.getUserById(1l).toString();
                }
            }));
        }
        int finishedThreads = 0;
        while (true) {
            for (int i = 0; i < futures.size(); i++) {
                if (futures.get(i).isDone()) {
                    try {
                        result.append(futures.get(i).get().toString());
                        result.append("<p>");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    futures.remove(i);
                    finishedThreads++;
                }
            }
            if (finishedThreads == threadsPool) {
                return result.toString();
            }
        }
    }

    @RequestMapping(APP_APPENDER_WITH_SEPARATOR + "testAll")
    public String testAll() {
        StringBuilder result = new StringBuilder();
        result.append(test1());
        result.append("<p>");
        result.append(test2());
        result.append("<p>");
        result.append(test3());
        result.append("<p>");
        result.append(test4());
        result.append("<p>");
        result.append(test5());
        result.append("<p>");
        result.append(test6());
        result.append("<p>");
        result.append(testMultithreading());
        result.append("<p>");
        return result.toString();
    }
}
