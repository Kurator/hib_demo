package borys.petrovskyi.hibernatedemo.entity;

import javax.persistence.Embeddable;

/**
 * Created by Kurator on 09.06.2017.
 */
@Embeddable
public class ContactInfo {

    private String email;
    private String skype;
    private String phone;

    protected ContactInfo() {
    }

    public ContactInfo(String email, String skype, String phone) {
        this.email = email;
        this.skype = skype;
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSkype() {
        return skype;
    }

    public void setSkype(String skype) {
        this.skype = skype;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public String toString() {
        return "ContactInfo{" +
                "email='" + email + '\'' +
                ", skype='" + skype + '\'' +
                ", phone='" + phone + '\'' +
                '}';
    }
}
