package borys.petrovskyi.hibernatedemo.entity;

import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * Created by Kurator on 11.06.2017.
 */
@Embeddable
public class OrderIdentifier implements Serializable {
    private Integer bill;
    private String invoice;

    protected OrderIdentifier() {
    }

    public OrderIdentifier(Integer billNumber, String invoice) {
        this.bill = billNumber;
        this.invoice = invoice;
    }

    public Integer getBillNumber() {
        return bill;
    }
    public void setBillNumber(Integer billNumber) {
        this.bill = billNumber;
    }
    public String getInvoice() {
        return invoice;
    }
    public void setInvoice(String invoice) {
        this.invoice = invoice;
    }

    @Override
    public String toString() {
        return "OrderIdentifier{" +
                "billNumber=" + bill +
                ", invoice='" + invoice + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OrderIdentifier that = (OrderIdentifier) o;
        if (!bill.equals(that.bill)) return false;
        return invoice.equals(that.invoice);

    }

    @Override
    public int hashCode() {
        int result = bill.hashCode();
        result = 31 * result + invoice.hashCode();
        return result;
    }

    public int compareTo(OrderIdentifier id) {
        return (bill < id.getBillNumber()) ? -1 : ((bill == id.getBillNumber()) ? 0 : 1);
    }
}
