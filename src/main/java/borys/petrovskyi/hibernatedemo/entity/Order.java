package borys.petrovskyi.hibernatedemo.entity;

import javax.persistence.*;

/**
 * Created by Kurator on 07.06.2017.
 */
@Entity
@Table(name = "orders")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(
        name = "type",
        discriminatorType = DiscriminatorType.STRING
)
@DiscriminatorValue(value = "regular")
public class Order {
    @EmbeddedId
    @AttributeOverrides(value = {
            @AttributeOverride(name = "bill", column = @Column(name="bill")),
            @AttributeOverride(name = "invoice", column = @Column(name="invoice"))
    })
    private OrderIdentifier id;
    @Column (name="user_id")
    private Integer userId;
    @Column(name="order")
    private Integer orderId;

    protected Order() {
    }

    public Order(OrderIdentifier id, Integer userId, Integer orderId, Integer bill, String invoice) {
        this.id = id;
        this.userId = userId;
        this.orderId = orderId;
        this.id = new OrderIdentifier(bill, invoice);
    }

    public OrderIdentifier getId() {
        return id;
    }
    public Integer getUserId() {
        return userId;
    }
    public Integer getOrderId() {
        return orderId;
    }
    public void setId(OrderIdentifier id) {
        this.id = id;
    }
    public void setUserId(Integer userId) {
        this.userId = userId;
    }
    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    @Override
    public String toString() {
        StringBuilder resString = new StringBuilder();
        resString.append("Order. ID = " + id + "; ");
        resString.append("UserID = " + userId + "; ");
        resString.append("OrderNumber = " + orderId + ";");
        return resString.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Order order = (Order) o;
        if (!id.equals(order.id)) return false;
        if (!userId.equals(order.userId)) return false;
        if (!orderId.equals(order.orderId)) return false;
        return true;
    }

    @Override
    public int hashCode() {
        int result = userId != null ? userId.hashCode() : 0;
        result = 31 * result + (orderId != null ? orderId.hashCode() : 0);
        return result;
    }
}
