package borys.petrovskyi.hibernatedemo.entity;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by Kurator on 11.06.2017.
 */
@Entity
@Table(name = "orders")
@DiscriminatorValue(value = "timed")
public class TimeLimitedOrder extends Order {
    @Column(name = "timelimit")
    private String date;

    public TimeLimitedOrder() {
    }

    public TimeLimitedOrder(OrderIdentifier id, Integer userId, Integer orderId, Integer bill, String invoice, String date) {
        super(id, userId, orderId, bill, invoice);
        this.date = date;
    }

    public String getDate() {
        return date;
    }
    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "TimeLimitedOrder{" +
                "date='" + date + '\'' +
                super.toString() + '}';
    }
}
