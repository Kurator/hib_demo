package borys.petrovskyi.hibernatedemo.entity;

import javax.persistence.*;

/**
 * Created by Kurator on 09.06.2017.
 */
@Entity
@Table(name = "addresses")
@SecondaryTable(name = "coordinates", pkJoinColumns = @PrimaryKeyJoinColumn(name = "id"))
public class Address {

    @Id
    @GeneratedValue
    private Long id;
    @Column(name = "country")
    private String country;
    @Column(name = "city")
    private String city;
    @Column(name = "street")
    private String street;
    @Column(name = "number")
    private String number;
    @Column(name = "user_id")
    private Long userId;
    @Column(table = "coordinates", name = "latitude")
    private String latitude;
    @Column(table = "coordinates", name = "longitude")
    private String longitude;

    public Address(String country, String city, String street, String number, Long userId, String latitude, String longitude) {
        this.country = country;
        this.city = city;
        this.street = street;
        this.number = number;
        this.userId = userId;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    protected Address() {
    }

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getCountry() {
        return country;
    }
    public void setCountry(String country) {
        this.country = country;
    }
    public String getCity() {
        return city;
    }
    public void setCity(String city) {
        this.city = city;
    }
    public String getStreet() {
        return street;
    }
    public void setStreet(String street) {
        this.street = street;
    }
    public String getNumber() {
        return number;
    }
    public void setNumber(String number) {
        this.number = number;
    }
    public String getLatitude() {
        return latitude;
    }
    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }
    public String getLongitude() {
        return longitude;
    }
    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }
    public Long getUserId() {
        return userId;
    }
    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "Address{" +
                "id=" + id +
                ", country='" + country + '\'' +
                ", city='" + city + '\'' +
                ", street='" + street + '\'' +
                ", number='" + number + '\'' +
                ", userId=" + userId +
                ", latitude='" + latitude + '\'' +
                ", longitude='" + longitude + '\'' +
                '}';
    }
}
