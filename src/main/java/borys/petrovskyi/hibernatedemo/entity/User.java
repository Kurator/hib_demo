package borys.petrovskyi.hibernatedemo.entity;

import javax.persistence.*;
import java.util.Set;

/**
 * Created by Kurator on 07.06.2017.
 */
@Entity
@Table(name="users")
public class User {

    @Id
    @GeneratedValue
    @Column(name="id")
    private Long id;
    @Column(name="username")
    private String userName;
    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "email", column = @Column(name="email")),
            @AttributeOverride(name = "skype", column = @Column(name="skype")),
            @AttributeOverride(name = "phone", column = @Column(name="phone"))
    })
    private ContactInfo contactInfo;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "userId", cascade = CascadeType.ALL)
    private Set<Order> userOrders;
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "userId", cascade = CascadeType.ALL)
    private Set<Address> userAddresses;

    protected User() {
    }

    public User(String username, ContactInfo info) {
        this.userName = username;
        this.contactInfo = info;
    }


    public Long getId() {
        return id;
    }
    public String getUserName() {
        return userName;
    }
    public Set<Order> getUserOrders() {
        return userOrders;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public void setUserName(String userName) {
        this.userName = userName;
    }
    public void setUserOrders(Set<Order> userOrders) {
        this.userOrders = userOrders;
    }
    public ContactInfo getContactInfo() {
        return contactInfo;
    }
    public void setContactInfo(ContactInfo contactInfo) {
        this.contactInfo = contactInfo;
    }
    public Set<Address> getUserAddresses() {
        return userAddresses;
    }
    public void setUserAddresses(Set<Address> userAddresses) {
        this.userAddresses = userAddresses;
    }

    @Override
    public String toString() {
        StringBuilder resString = new StringBuilder();
        resString.append("User. ID = " + id + "; ");
        resString.append("UserName = " + userName + "; ");
        resString.append("ContactInfo = " + contactInfo.toString() + ";");
        return resString.toString();
    }
}
