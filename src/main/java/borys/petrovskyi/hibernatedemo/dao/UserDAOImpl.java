package borys.petrovskyi.hibernatedemo.dao;

import borys.petrovskyi.hibernatedemo.entity.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Kurator on 08.06.2017.
 */
@Service("UserDAOImpl")
public class UserDAOImpl implements UserDAO {
    private SessionFactory sessionFactory;
    private Session currentSession;

    public UserDAOImpl() {
        StandardServiceRegistry standardRegistry = new StandardServiceRegistryBuilder().configure("hibernate.cfg.xml").build();
        Metadata metaData = new MetadataSources(standardRegistry).getMetadataBuilder().build();
        sessionFactory = metaData.getSessionFactoryBuilder().build();
        currentSession = sessionFactory.openSession();
    }

    @Override
    public List<User> findAll() {
        return currentSession.createCriteria(User.class).setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY).list();
    }

    @Override
    public User findOne(Long id) {
        return currentSession.get(User.class, id);
    }

    @Override
    public void addUser(User user) {
        currentSession.save(user);
    }

    @Override
    public void delete(User user) {
        currentSession.delete(user);
    }

    @Override
    public User findByName(String name) {
        return (User) currentSession.createCriteria(User.class).add(Restrictions.eq("userName", name)).list().get(0);
    }

    @Override
    public User saveAndFlush(User user) {
        return (User) currentSession.merge(user);
    }

    @Override
    public void closeSession() {
        currentSession.close();
        sessionFactory.close();
    }
}
