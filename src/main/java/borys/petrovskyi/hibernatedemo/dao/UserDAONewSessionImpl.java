package borys.petrovskyi.hibernatedemo.dao;

import borys.petrovskyi.hibernatedemo.entity.User;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Kurator on 11.06.2017.
 */
@Service("UserDAONewSessionImpl")
public class UserDAONewSessionImpl implements UserDAO {
    private SessionFactory sessionFactory;

    public UserDAONewSessionImpl() {
        StandardServiceRegistry standardRegistry = new StandardServiceRegistryBuilder().configure("hibernate.cfg.xml").build();
        Metadata metaData = new MetadataSources(standardRegistry).getMetadataBuilder().build();
        sessionFactory = metaData.getSessionFactoryBuilder().build();
    }

    @Override
    public List<User> findAll() {
        try (Session currentSession = getSession()) {
            return currentSession.createCriteria(User.class).setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY).list();
        }
    }

    @Override
    public User findOne(Long id) {
        try (Session currentSession = getSession()) {
            User user = currentSession.get(User.class, id);
            Hibernate.initialize(user.getUserOrders());
            return user;
        }
    }

    @Override
    public void addUser(User user) {
        try (Session currentSession = getSession()) {
            currentSession.save(user);
        }
    }

    @Override
    public void delete(User user) {
        try (Session currentSession = getSession()) {
            currentSession.delete(user);
        }
    }

    @Override
    public User findByName(String name) {
        try (Session currentSession = getSession()) {
            User user = (User) currentSession.createCriteria(User.class).add(Restrictions.eq("userName", name)).list().get(0);
            Hibernate.initialize(user.getUserOrders());
            /*try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }*/
            return user;
        }
    }

    @Override
    public User saveAndFlush(User user) {
        try (Session currentSession = getSession()) {
            return (User) currentSession.merge(user);
        }
    }

    @Override
    public void closeSession() {
        sessionFactory.close();
    }

    private Session getSession() {
        return sessionFactory.openSession();
    }
}
