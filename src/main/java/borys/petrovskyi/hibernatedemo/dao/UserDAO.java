package borys.petrovskyi.hibernatedemo.dao;

import borys.petrovskyi.hibernatedemo.dinterface.ICloseSession;
import borys.petrovskyi.hibernatedemo.entity.User;

import java.util.List;

/**
 * Created by Kurator on 08.06.2017.
 */
public interface UserDAO extends ICloseSession {
    List<User> findAll();
    User findOne(Long id);
    void addUser(User user);
    void delete(User user);
    User findByName(String name);
    User saveAndFlush(User user);
}
