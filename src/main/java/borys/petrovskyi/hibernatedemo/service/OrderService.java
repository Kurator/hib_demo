package borys.petrovskyi.hibernatedemo.service;

import borys.petrovskyi.hibernatedemo.entity.Order;

import java.util.List;

/**
 * Created by Kurator on 08.06.2017.
 */
public interface OrderService {
    Order getOrderById(Long id);
    List<Order> getOrdersByUserId(Integer id);
    List<Order> getAll();
}
