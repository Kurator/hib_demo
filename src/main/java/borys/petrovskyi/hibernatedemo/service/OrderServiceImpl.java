package borys.petrovskyi.hibernatedemo.service;

import borys.petrovskyi.hibernatedemo.entity.Order;
import borys.petrovskyi.hibernatedemo.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Kurator on 08.06.2017.
 */
@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderRepository orderRepository;

    @Override
    public Order getOrderById(Long id) {
        return orderRepository.findOne(id);
    }

    @Override
    public List<Order> getOrdersByUserId(Integer id) {
        return orderRepository.findOrdersByUserId(id);
    }

    @Override
    public List<Order> getAll() {
        return orderRepository.findAll();
    }
}
