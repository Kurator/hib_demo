package borys.petrovskyi.hibernatedemo.service;

import borys.petrovskyi.hibernatedemo.dinterface.ICloseSession;
import borys.petrovskyi.hibernatedemo.entity.User;

import java.util.List;

/**
 * Created by Kurator on 07.06.2017.
 */
public interface UserService {
    User addUser(User user);
    void delete(User user);
    User getByName(String name);
    User getUserById(Long id);
    User editUser(User user);
    List<User> getAllUsers();
    void closeSession();
}
