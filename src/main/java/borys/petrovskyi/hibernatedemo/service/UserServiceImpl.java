package borys.petrovskyi.hibernatedemo.service;

import borys.petrovskyi.hibernatedemo.dao.UserDAO;
import borys.petrovskyi.hibernatedemo.dao.UserDAONewSessionImpl;
import borys.petrovskyi.hibernatedemo.entity.User;
import borys.petrovskyi.hibernatedemo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Kurator on 07.06.2017.
 */
@Service
public class UserServiceImpl implements UserService {
    @Autowired
    //private UserRepository userRepository;
    @Qualifier("UserDAONewSessionImpl")
    private UserDAO userRepository;

    @Override
    public User addUser(User user) {
        return userRepository.saveAndFlush(user);
    }

    @Override
    public void delete(User user) {
        userRepository.delete(user);
    }

    @Override
    public User getByName(String name) {
        return userRepository.findByName(name);
    }

    @Override
    public User getUserById(Long id) {
        return userRepository.findOne(id);
    }

    @Override
    public User editUser(User user) {
        return userRepository.saveAndFlush(user);
    }

    @Override
    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    @Override
    public void closeSession() {
        userRepository.closeSession();
    }
}
