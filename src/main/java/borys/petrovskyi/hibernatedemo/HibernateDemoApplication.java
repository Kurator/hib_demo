package borys.petrovskyi.hibernatedemo;

import borys.petrovskyi.hibernatedemo.controller.TestController;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.*;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@SpringBootApplication
@EnableAutoConfiguration
@ComponentScan({"borys.petrovskyi.hibernatedemo.service", "borys.petrovskyi.hibernatedemo.controller", "borys.petrovskyi.hibernatedemo.dao"})
@EnableJpaRepositories("borys.petrovskyi.hibernatedemo.repository")
@PropertySource("classpath:application.properties")
public class HibernateDemoApplication {

	public static void main(String[] args) {
		/*try (AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(HibernateDemoApplication.class)) {
			TestController tc = ctx.getBean(TestController.class);
			tc.startTest();
		}*/
		SpringApplication.run(HibernateDemoApplication.class, args);
	}

}
