package borys.petrovskyi.hibernatedemo.dinterface;

/**
 * Created by Kurator on 08.06.2017.
 */
public interface ICloseSession {
    default void closeSession() {
        System.out.println("Session has been closed");
    }
}
